    //
    // Создайте приложение секундомер.
    //
    //     * Секундомер будет иметь 3 кнопки "Старт, Стоп, Сброс"
    //     * При нажатии на кнопку стоп фон секундомера должен быть красным, старт - зеленый, сброс - серый
    //     * Вывод счётчиков в формате ЧЧ:ММ:СС<br>
    //     * Реализуйте Задание используя синтаксис ES6 и стрелочные функции
    let counter = 0;
    let id;
    let buttons = document.querySelectorAll('button');
    let spans = document.querySelectorAll('span');
    let spanHours = spans[0];
    let spanMinutes = spans[1];
    let spanSeconds = spans[2];
    let startBt = buttons[0];
    let stopBt = buttons[1];
    let resetBt = buttons[2];
    let display = document.querySelector('.stopwatch-display');


    function  tick () {
        let s = parseInt(spanSeconds.innerText);
        let m = parseInt(spanMinutes.innerText);
        let h = parseInt(spanHours.innerText);
        s += 1;
       if (s == 60) {
            s = '00'
            m += 1;
            }
       if (m === 60) {
           m = '00';
           m += 1;
       }
       if (h === 24) {
           h = '00'
       }
       if (s < 10) {
           s = '0' + s;
       }
        if (m < 10) {
            m = '0' + m;
        }
        if (h < 10) {
            h = '0' + h;
        }
        spanSeconds.innerText = s;
        spanMinutes.innerText = m;
        spanHours.innerText = h;
    };

    function timer () {
        if ( counter == 1) {
            id = setInterval(tick, 1000);
        }
    };


    startBt.onclick = () => {
        counter += 1;
        display.classList.remove('red', 'silver')
        display.classList.add('green');
        timer();
    };

    stopBt.onclick = () => {
        counter = 0;
        display.classList.remove('green', 'silver');
        display.classList.add('red');
        clearInterval(id);
    };

    resetBt.onclick = () => {
        counter = 0;
        display.classList.remove('green', 'red');
        display.classList.add('silver');
        spanSeconds.innerText ='00';
        spanHours.innerText = '00';
        spanMinutes.innerText = '00';
        clearInterval(id);
    }



