
// Реализуйте программу проверки телефона<br>
// * Используя JS Создайте поле для ввода телефона и кнопку сохранения
// * Пользователь должен ввести номер телефона в формате 000-000-00-00
//
// * Поле того как пользователь нажимает кнопку сохранить проверте правильность ввода номера, если номер
// правильный
// сделайте зеленый фон и используя document.location переведите пользователя на страницу
// https://risovach.ru/upload/2013/03/mem/toni-stark_13447470_big_.jpeg
// если будет ошибка отобразите её в диве до input.

function createEll(tag) {
    return document.createElement(tag);
};

let body = document.querySelector('body');
body.append(createEll('input'), createEll('button'));

let button = document.querySelector('button');
button.innerText = 'Сохранить';

let input = document.querySelector('input');
const pattern = /[0-9]{3}-[0-9]{3}-[0-9]{2}-[0-9]{2}/;

button.onclick = () => {
    let text = input.value;
    let result = text.search(pattern)
    if (result == -1) {
        body.prepend(createEll('div'));
        let div = document.querySelector('div');
        div.innerHTML = 'Ошибка, неверный формат!';
    } else {
        body.classList.add('green');
        document.location = 'https://risovach.ru/upload/2013/03/mem/toni-stark_13447470_big_.jpeg'
    }
}