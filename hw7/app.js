// Реализовать функцию для создания объекта "пользователь".
// Написать функцию createNewUser(), которая будет создавать и возвращать объект newUser. При вызове функция должна спросить
// у вызывающего имя и фамилию. Используя данные, введенные пользователем, создать объект newUser со свойствами firstName и
// lastName. Добавить в объект newUser метод getLogin(), который будет возвращать первую букву имени пользователя, соединенную
// с фамилией пользователя, все в нижнем регистре (например, Ivan Kravchenko → ikravchenko). Создать пользователя с помощью
// функции createNewUser(). Вызвать у пользователя функцию getLogin(). Вывести в консоль результат выполнения функции.,

// Дополнить функцию createNewUser() методами подсчета возраста пользователя и его паролем.
//     Возьмите выполненное задание выше (созданная вами функция createNewUser()) и дополните ее следующим функционалом:
//     При вызове функция должна спросить у вызывающего дату рождения (текст в формате dd.mm.yyyy) и сохранить ее в поле
//     birthday. Создать метод getAge() который будет возвращать сколько пользователю лет. Создать метод getPassword(),
//     который будет возвращать первую букву имени пользователя в верхнем регистре, соединенную с фамилией
//     (в нижнем регистре) и годом рождения. (например, Ivan Kravchenko 13.03.1992 → Ikravchenko1992). Вывести
//     в консоль результат работы функции createNewUser(), а также функций getAge() и getPassword() созданного объекта.

const firstName = prompt('Введите ваше имя');
const lastName = prompt('Введите вашу фамилию');

function getBirthday() {
    let birthday = prompt('Введите вашу дату рождения', 'dd.mm.yyyy');
    while (birthday[2] !== '.' || birthday[5] !== '.' || birthday.length !== 10) {
        birthday = prompt('Неверный формат! Пожалуйста, введите вашу дату рождения в формате dd.mm.yyyy');
    };
    return birthday
}


class createNewUser {
    constructor () {
       this.firstName = firstName;
       this.lastName = lastName;
       this.birthday = getBirthday();
    }
    getLogin() {
        if (this.firstName !== undefined || this.birthday !== undefined) {
            return (this.firstName[0] + this.lastName).toLowerCase();
        } else  alert('Пользователь не внёс данные');
    }
    getAge() {
        const todayDate = new Date;
        const todayDay = todayDate.getUTCDate();
        const todayMonth = todayDate.getMonth();
        const todayYear = todayDate.getFullYear();
        const birthdayDay = this.birthday.substr(0,2);
        const birthdayMonth = this.birthday.substr(3, 2);
        const birthdayYear = this.birthday.substr(6, 4);
        let age = todayYear - birthdayYear;
        if (birthdayMonth > (todayMonth + 1)) {
            age -= 1;
        } else if ((todayMonth + 1) == birthdayMonth && birthdayDay > todayDay) {
            age -= 1;
        }
        return age;
    }
    getPassword() {
        const birthdayYear = this.birthday.substr(6, 4);
        return this.firstName[0].toUpperCase() + this.lastName.toLowerCase() + birthdayYear;
    }
}


const newUser = new createNewUser();

console.log(newUser.getLogin());
console.log(newUser.getAge());
console.log(newUser.getPassword());

//     Реализовать функцию фильтра массива по указанному типу данных.
//     Написать функцию filterBy(), которая будет принимать в себя 2 аргумента. Первый аргумент - массив, который
//     будет содержать в себе любые данные, второй аргумент - тип данных. Функция должна вернуть новый массив, который
//     будет содержать в себе все данные, которые были переданы в аргумент, за исключением тех, тип которых был передан
//     вторым аргументом. То есть, если передать массив ['hello', 'world', 23, '23', null], и вторым аргументом передать
//     'string', то функция вернет массив [23, null].

 const myArray = [11, 12, 34, '66', '78', 1, 'Bjnd', null, undefined];
function filterBy(array, dataType) {
    let newArray = array.filter(function (item){
        return typeof item !== dataType;
    })
    return newArray;
}
console.log(filterBy(myArray, 'number'));