//Рисовать на странице круг используя параметры, которые введет пользователь.
// При загрузке страницы - показать на ней кнопку с текстом "Нарисовать круг".
//Данная кнопка должна являться единственным контентом в теле HTML документа, весь остальной контент должен быть
// создан и добавлен на страницу с помощью Javascript
// При нажатии на кнопку "Нарисовать круг" показывать одно поле ввода - диаметр круга. При нажатии на кнопку
// "Нарисовать" создать на странице 100 кругов (10*10) случайного цвета. При клике на конкретный круг - этот
// круг должен исчезать, при этом пустое место заполняться, то есть все остальные круги сдвигаются влево.

const body = document.querySelector('body');
const button = document.querySelector('button');
button.onclick = function () {
    const diameter = prompt('Введите диаметр круга');

    for (let i = 0; i < 100; i++){
        body.append(document.createElement("div"));
    };

    const circles = document.querySelectorAll('div');

    circles.forEach(function (item, i) {
        circles[i].style.backgroundColor = 'red';
        circles[i].style.width = `${diameter}px`;
        circles[i].style.height = `${diameter}px`;
        circles[i].style.borderRadius = `${diameter}px`;
        circles[i].onclick = function () {
            circles[i].remove();
        };
    })
};