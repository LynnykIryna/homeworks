
    const someInf = {
        "date": "26.10.2021",
        "bank": "PB",
        "baseCurrency": 980,
        "baseCurrencyLit": "UAH",
        "exchangeRate":
            [{"baseCurrency": "UAH", "saleRateNB": 19.7289000, "purchaseRateNB": 19.7289000},
                {"baseCurrency": "UAH", "currency": "AZN", "saleRateNB": 15.6620000, "purchaseRateNB": 15.6620000},
                {"baseCurrency": "UAH", "currency": "BYN", "saleRateNB": 10.8978000, "purchaseRateNB": 10.8978000},
                {"baseCurrency": "UAH", "currency": "CAD", "saleRateNB": 21.3186000, "purchaseRateNB": 21.3186000},
                {
                    "baseCurrency": "UAH",
                    "currency": "CHF",
                    "saleRateNB": 28.6688000,
                    "purchaseRateNB": 28.6688000,
                    "saleRate": 29.2000000,
                    "purchaseRate": 27.6000000
                },
                {"baseCurrency": "UAH", "currency": "CNY", "saleRateNB": 4.1236000, "purchaseRateNB": 4.1236000},
                {
                    "baseCurrency": "UAH",
                    "currency": "CZK",
                    "saleRateNB": 1.1889000,
                    "purchaseRateNB": 1.1889000,
                    "saleRate": 1.3000000,
                    "purchaseRate": 1.1000000
                },
                {"baseCurrency": "UAH", "currency": "DKK", "saleRateNB": 4.1098000, "purchaseRateNB": 4.1098000},
                {
                    "baseCurrency": "UAH",
                    "currency": "EUR",
                    "saleRateNB": 30.5749000,
                    "purchaseRateNB": 30.5749000,
                    "saleRate": 30.8500000,
                    "purchaseRate": 30.2500000
                },
                {
                    "baseCurrency": "UAH",
                    "currency": "GBP",
                    "saleRateNB": 36.2496000,
                    "purchaseRateNB": 36.2496000,
                    "saleRate": 37.2000000,
                    "purchaseRate": 35.2000000
                },
                {"baseCurrency": "UAH", "currency": "HUF", "saleRateNB": 0.0835770, "purchaseRateNB": 0.0835770},
                {"baseCurrency": "UAH", "currency": "ILS", "saleRateNB": 8.2218000, "purchaseRateNB": 8.2218000},
                {"baseCurrency": "UAH", "currency": "JPY", "saleRateNB": 0.2317300, "purchaseRateNB": 0.2317300},
                {"baseCurrency": "UAH", "currency": "KZT", "saleRateNB": 0.0619540, "purchaseRateNB": 0.0619540},
                {"baseCurrency": "UAH", "currency": "MDL", "saleRateNB": 1.5081000, "purchaseRateNB": 1.5081000},
                {"baseCurrency": "UAH", "currency": "NOK", "saleRateNB": 3.1517000, "purchaseRateNB": 3.1517000},
                {
                    "baseCurrency": "UAH",
                    "currency": "PLN",
                    "saleRateNB": 6.6218000,
                    "purchaseRateNB": 6.6218000,
                    "saleRate": 6.8100000,
                    "purchaseRate": 6.5100000
                },
                {
                    "baseCurrency": "UAH",
                    "currency": "RUB",
                    "saleRateNB": 0.3772000,
                    "purchaseRateNB": 0.3772000,
                    "saleRate": 0.3890000,
                    "purchaseRate": 0.3600000
                },
                {"baseCurrency": "UAH", "currency": "SEK", "saleRateNB": 3.0582000, "purchaseRateNB": 3.0582000},
                {"baseCurrency": "UAH", "currency": "SGD", "saleRateNB": 19.5583000, "purchaseRateNB": 19.5583000},
                {"baseCurrency": "UAH", "currency": "TMT", "saleRateNB": 7.6050000, "purchaseRateNB": 7.6050000},
                {"baseCurrency": "UAH", "currency": "TRY", "saleRateNB": 2.7277000, "purchaseRateNB": 2.7277000},
                {"baseCurrency": "UAH", "currency": "UAH", "saleRateNB": 1.0000000, "purchaseRateNB": 1.0000000},
                {
                    "baseCurrency": "UAH",
                    "currency": "USD",
                    "saleRateNB": 26.3509000,
                    "purchaseRateNB": 26.3509000,
                    "saleRate": 26.6500000,
                    "purchaseRate": 26.2500000
                },
                {"baseCurrency": "UAH", "currency": "UZS", "saleRateNB": 0.0024895, "purchaseRateNB": 0.0024895},
                {"baseCurrency": "UAH", "currency": "GEL", "saleRateNB": 8.5403000, "purchaseRateNB": 8.5403000}]
    }


    const root = document.getElementById('root');
    const table = document.createElement('table');
    root.prepend(table);


    someInf.exchangeRate.forEach(function (item, index) {
            const tr = document.createElement('tr');
            table.append(tr);
            for (let key in item) {
                if (index === 0 || index === 1 || index === 2 || index === 3) {
                    continue;
                } else if (index === 4) {
                    const th = document.createElement('th');
                    th.innerHTML = `${key}`;
                    tr.append(th);
                } else {
                    const td = document.createElement('td');
                    td.innerHTML = `${item[key]}`;
                    tr.append(td);
                }
            }
        }
    );




