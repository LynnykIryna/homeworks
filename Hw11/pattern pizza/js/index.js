let bill = document.querySelector('.price > p');
let [...radio] = document.querySelectorAll('.radioIn');
let sizePrice = 0;
let totalPrice = 0;

radio.forEach((item) => {
   item.addEventListener('change', () => {
       if ( item.value == "small" ) {
           sizePrice = 100
       } else if ( item.value == "mid" ) {
           sizePrice = 150
       } else {
           sizePrice = 200
       }
       totalPrice += sizePrice;
       bill.innerHTML = `ЦІНА: ${totalPrice}`
   })
})


let [...ingridients] = document.querySelectorAll('.draggable');
let pizza = document.querySelector('.table');

ingridients.forEach((item) => { item.addEventListener('dragstart', dragStart) });
pizza.addEventListener('dragover', dragOver);
pizza.addEventListener('dragenter', dragEnter);
pizza.addEventListener('dragleave', dragLeave);
pizza.addEventListener('drop', dragDrop);


function dragStart () {
    this.setAttribute('id', 'active');
}

function dragOver (e) {
    e.preventDefault();
}

function  dragEnter () {
    pizza.classList.add('hovered');
}

function dragLeave () {
    pizza.classList.remove('hovered');
}


let addedSauces = document.querySelector('.sauces > p');
let addedTopings = document.querySelector('.topings > p');

function dragDrop (e) {
    let ingr = document.querySelector('#active');
    let ingrClone = ingr.cloneNode();

    ingr.removeAttribute('id');
    pizza.append(ingrClone);
    pizza.classList.remove('hovered');

    let priceOfIngr = Number(ingr.getAttribute('data-price'));

    totalPrice += priceOfIngr;
    bill.innerHTML = `ЦІНА: ${totalPrice}`

    if (ingr.getAttribute('data-name') === 'sauce') {
        addedSauces.innerHTML += ` ${ingr.getAttribute('data-value')};`
    } else if (ingr.getAttribute('data-name') === 'toping') {
        addedTopings.innerHTML += ` ${ingr.getAttribute('data-value')};`
    }
}


let bannerButton = document.querySelector('#banner > button');

bannerButton.addEventListener('mouseover', () => {

    let banner = document.querySelector('#banner');

    banner.style.bottom = randomInt(1, 80) + '%';
    banner.style.right = randomInt(1, 80) + '%';
} )


function randomInt (min, max) {
    return Math.random() * (max - min) + min;
}


let email = document.querySelector("input[type = 'email']")
let phoneNum = document.querySelector('input[name = "phone"]')
let name = document.querySelector('input[name = "name"]')

email.addEventListener('change', () => {
    if (name.value.search(/\w{2}/) === -1 ) {
        name.classList.add('error')
    } else {
        name.classList.remove('error')
    }
})

phoneNum.addEventListener('change', () => {
    if ( phoneNum.value.search(/\+380\d{9}/) === -1 ) {
        phoneNum.classList.add('error')
    } else {
        phoneNum.classList.remove('error')
    }
})

email.addEventListener('change', () => {
    if (email.value.search(/.{3}@/) === -1) {
        email.classList.add('error')
    } else {
        email.classList.remove('error')
    }
})


let submitButton = document.querySelector('input[name = "btnSubmit"]')

submitButton.addEventListener('click', () => {
    if (name.value.search(/\w{2}/) === -1  || phoneNum.value.search(/\+380\d{9}/) === -1 ||
        email.value.search(/.{3}@/) === -1) {
     alert('Неверные данные')
   } else {
     window.location.href ="./thank-you.html"
 }
})


