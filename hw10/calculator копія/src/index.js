
const calc = {
    operand1: "",
    operand2: "",
    sign: "",
    result: '',
    mrc: ""
};

window.addEventListener("DOMContentLoaded", () => {
    const btns = document.querySelector(".keys");
    btns.addEventListener("click", click)
})

const click = (e) => {

    //если сначала нажали на цифру
    if (/[0-9.]/.test(e.target.value) && calc.sign === "") {
        calc.operand1 += e.target.value
        show(calc.operand1)
    }

    //если вторым нажали на оперант
    else if (/[+*-/]/.test(e.target.value) && /m/.test(e.target.value) == false && calc.operand1 !== '') {
        calc.sign = e.target.value
    }

    //если 3-м нажали на цифру
    else if (/[0-9.]/.test(e.target.value) && calc.sign !== "") {
        calc.operand2 += e.target.value
        hide();
        show(calc.operand2)
    }

    //если нажали на равно
    else if (/=/.test(e.target.value) && calc.sign !== '' && calc.operand2 !== '') {
        hide()
        if (calc.sign == '-') {
            calc.result = parseInt(calc.operand1) - parseInt(calc.operand2);
        } else if (calc.sign == '+') {
            calc.result = parseInt(calc.operand1) + parseInt(calc.operand2);
        } else if (calc.sign == '*') {
            calc.result = parseInt(calc.operand1) * parseInt(calc.operand2);
        } else {
            calc.result = parseInt(calc.operand1) / parseInt(calc.operand2);
        }
    show(calc.result);
        calc.operand1 = '';
        calc.operand2 = '';
        calc.sign = '';
    }

    //если нажали на сброс (С)
    else if (/C/.test(e.target.value)) {
        hide()
        calc.operand1 = '';
        calc.operand2 = '';
        calc.sign = '';
        calc.mrc = '';
        document.querySelector('span').innerHTML = ''
    }

    //если нажали на m+
    else if (/m+/.test(e.target.value) && /[r-]/.test(e.target.value) == false && calc.result !== '') {
        document.querySelector('span').innerHTML = 'm';
        if (calc.mrc !== '') {
            calc.mrc += calc.result
        } else {
            calc.mrc = calc.result;
        }
    }

    //если нажали на m-
    else if (/m-/.test(e.target.value) && /[r+]/.test(e.target.value) == false && calc.result !== '') {
        document.querySelector('span').innerHTML = 'm';
        if (calc.m !== '') {
            calc.mrc -= calc.result
        } else {
            calc.mrc = calc.result;
        }
    }

    ////если нажали на mrc
    else if (/mrc/.test(e.target.value) && calc.mrc !== '') {
        show(calc.mrc)
    } else {
        console.error("error")
    }
}

 show = (data) => {
    document.querySelector(".display > input").value = data
}
 hide = () => {
     document.querySelector(".display > input").value = ''
 }
